package com.example.invate.myapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
// Need to implement adapter view so that the spinner works
public class BuyButtonActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_button);
        // Spinner element
        Spinner spinner = (Spinner) findViewById(R.id.spinner3 );

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("1 VIP Tickets-£15");
        categories.add("2 VIP Tickets-£30 ");
        categories.add("3 VIP Tickets-£45");
        categories.add("1 Regular Tickets-£8");
        categories.add("2 Regular Tickets-£16");
        categories.add("3 Regular Tickets-£24");



        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);

    }
    // This is the button that directs the user to the credit card details interface
    public void BuyButton2(View v){
        Intent intent=new Intent(BuyButtonActivity.this, CreditCardActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("Key1", "this is the message");
        startActivity(intent);
    }



    // This is automatically generated when adding the spinner widget where it will show what you have selected (if any)
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}


