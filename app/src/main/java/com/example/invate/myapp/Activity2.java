package com.example.invate.myapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
// Need to implement adapter view for the spinner to be imported
public class Activity2 extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Artist Interface
        setContentView(R.layout.activity_2);
        // Spinner element
        Spinner spinner = (Spinner) findViewById(R.id.spinner2 );

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Katy Perry");
        categories.add("Tinie Tempah");
        categories.add("Akon");
        categories.add("The Wanted");
        categories.add("One Direction");
        categories.add("Little Mix");
        categories.add("Taylor Swift");
        categories.add("Drake");
        categories.add("Eminem");
        categories.add("Coldplay");
        categories.add("Chris Brown");
        categories.add("Ed Sheeran");
        categories.add("Jason Derulo");
        categories.add("Calvin Harris");
        categories.add("Sia");
        categories.add("Rudimental");
        categories.add("Bruno Mars");
        categories.add("Rihanna");
        categories.add("David Guetta");


        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
    }

// This part of the code will return the user back to the menu screen
    public void ButtonNumber2(View v){
        Intent intent=new Intent(Activity2.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("Key1", "this is the message");
        startActivity(intent);
    }

    // This is automatically generated when adding the spinner widget where it will show what you have selected (if any)
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

