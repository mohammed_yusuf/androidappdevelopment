package com.example.invate.myapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class PremiumActivity2 extends AppCompatActivity {
    EditText cardholder;
    EditText cardNumber;
    EditText expiry;
    EditText cvc;
    public static final String cardholderKey = "cardHolKey";
    public static final String cardNumberKey = "cardNumberKey";
    public static final String expiryKey = "expKey";
    public static final String cvcKey = "cvKey";
    SharedPreferences sharedPreferences;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premium2);
        cardholder = (EditText) findViewById(R.id.cardholderText);
        cardNumber = (EditText) findViewById(R.id.cardNumberText);
        expiry = (EditText) findViewById(R.id.expiryText);
        cvc = (EditText) findViewById(R.id.cvcText);
        sharedPreferences = getSharedPreferences("MySharedPreMain", Context.MODE_PRIVATE);
        if (sharedPreferences.contains(cardholderKey)){
            cardholder.setText(sharedPreferences.getString(cardholderKey,""));
        }
        if (sharedPreferences.contains(cardNumberKey)){
            cardNumber.setText(sharedPreferences.getString(cardNumberKey,""));
        }
        if (sharedPreferences.contains(expiryKey)){
            expiry.setText(sharedPreferences.getString(expiryKey, ""));
        }
        if (sharedPreferences.contains(cvcKey)){
            cvc.setText(sharedPreferences.getString(cvcKey,""));
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public void SaveButton(View v){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(cardholderKey, cardholder.getText().toString());
        editor.putString(cardNumberKey, cardNumber.getText().toString());
        editor.putString(expiryKey, expiry.getText().toString());
        editor.putString(cvcKey, cvc.getText().toString());
        editor.commit();
        Toast.makeText(this, "Data Saved", Toast.LENGTH_SHORT).show();

    }


    public void CreditButton(View v){
        Intent intent=new Intent(PremiumActivity2.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("Key1", "this is the message");
        startActivity(intent);
    }
    public void FinishButton(View v){


        Intent intent=new Intent(PremiumActivity2.this, FinishActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("Key1", "this is the message");
        startActivity(intent);

    }

}
