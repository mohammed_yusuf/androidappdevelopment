package com.example.invate.myapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

public class PremiumActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // upgrade to premium interface
        setContentView(R.layout.activity_premium);
    }


// This will return the user back to the menu screen
    public void PremiumButton(View v) {
        Intent intent = new Intent(PremiumActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("Key1", "this is the message");
        startActivity(intent);
    }

}
