package com.example.invate.myapp;

import android.app.ListActivity;
import android.content.ClipData;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class Activity6 extends ListActivity {
    // Displays the activities as a list need to implement a ListView in xml for this to work
    String[] Activities ={
            "Crossword",
            "Word search",
            "Spot the difference"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_6);
        // Weekly competition interface
        Button ButtonNumber6 = (Button) findViewById(R.id.button5);
        setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Activities));
    }
    // This will display the activity the user has selected from the weekly list
    public void onListItemClick(ListView l, View v, int position, long id){
        Toast.makeText(this, "Your activity this time is " + Activities[position], Toast.LENGTH_SHORT).show();
    }
    // This button is used to return back to menu
    public void ButtonNumber6(View v){
        Intent intent=new Intent(Activity6.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("Key1", "this is the message");
        startActivity(intent);
    }
}
