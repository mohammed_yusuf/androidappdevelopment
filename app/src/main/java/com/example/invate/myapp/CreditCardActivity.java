package com.example.invate.myapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class CreditCardActivity extends AppCompatActivity {
    // Declaring EditText variables
    EditText cardholder;
    EditText cardNumber;
    EditText expiry;
    EditText cvc;
    EditText answer;
    // This declares the key-value pairs each key-value pair is a constant
    public static final String cardholderKey = "cardHolderKey";
    public static final String cardNumberKey = "cardNumberKey";
    public static final String expiryKey = "expiryKey";
    public static final String cvcKey = "cvcKey";
    public static final String answerKey = "answerKey";
    SharedPreferences sharedPreferences;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_card);
        // This part of the code is to determine the EditText id
        cardholder = (EditText) findViewById(R.id.cardholderText);
        cardNumber = (EditText) findViewById(R.id.cardNumberText);
        expiry = (EditText) findViewById(R.id.expiryText);
        cvc = (EditText) findViewById(R.id.cvcText);
        answer = (EditText) findViewById(R.id.answerText);
        sharedPreferences = getSharedPreferences("MySharedPreMains", Context.MODE_PRIVATE);
        // If there is any input in the card holder section then the data will be stored
        if (sharedPreferences.contains(cardholderKey)){
            cardholder.setText(sharedPreferences.getString(cardholderKey,""));
        }
        // If there is any input in the card number section then the data will be stored
        if (sharedPreferences.contains(cardNumberKey)){
            cardNumber.setText(sharedPreferences.getString(cardNumberKey,""));
        }
        // If there is any input in the expiry section then the data will be stored
        if (sharedPreferences.contains(expiryKey)){
            expiry.setText(sharedPreferences.getString(expiryKey, ""));
        }
        // If there is any input in the cvc section then the data will be stored
        if (sharedPreferences.contains(cvcKey)){
            cvc.setText(sharedPreferences.getString(cvcKey,""));
        }
        // If there is any input in the answer (upgrade to premium) section then the data will be stored
        if (sharedPreferences.contains(answerKey)){
            cvc.setText(sharedPreferences.getString(answerKey,""));
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    // This is the class that is needed to save the inputted data
    public void SaveButton(View v){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(cardholderKey, cardholder.getText().toString());
        editor.putString(cardNumberKey, cardNumber.getText().toString());
        editor.putString(expiryKey, expiry.getText().toString());
        editor.putString(cvcKey, cvc.getText().toString());
        editor.putString(answerKey, cvc.getText().toString());
        editor.commit();
        Toast.makeText(this, "Data Saved", Toast.LENGTH_SHORT).show();

    }

    // This returns the user back to the menu section
    public void CreditButton(View v){
        Intent intent=new Intent(CreditCardActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("Key1", "this is the message");
        startActivity(intent);
    }
    // This button button will direct the user to the finish screen
    public void FinishButton(View v){


        Intent intent=new Intent(CreditCardActivity.this, FinishActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("Key1", "this is the message");
        startActivity(intent);

    }

}
